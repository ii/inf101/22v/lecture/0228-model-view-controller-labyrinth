package inf101v22.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import inf101v22.model.Labyrinth;
import inf101v22.view.View;

public class Controller implements KeyListener {

    private View view;
    private Labyrinth model;

    public Controller(View view, Labyrinth model) {
        this.view = view;
        this.model = model;
        this.view.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            this.model.moveDown();
        }
        
        this.view.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }
    
}
