package inf101v22;

import javax.swing.JFrame;

import inf101v22.controller.Controller;
import inf101v22.model.Labyrinth;
import inf101v22.view.View;

public class Main {
    
    public static void main(String[] args) {
        Labyrinth model = new Labyrinth();
        View view = new View(model);
        new Controller(view, model);
        
        JFrame frame = new JFrame("INF101");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(view);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }
}
