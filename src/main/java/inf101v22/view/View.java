package inf101v22.view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

import inf101v22.grid.Location;
import inf101v22.model.Labyrinth;

public class View extends JComponent {

    private Labyrinth model;

    public View(Labyrinth model) {
        this.setFocusable(true); // denne linjen glemte jeg i forelesning -- den må kalles for at View-komponenten skal kunne motta tastetrykk.
        this.model = model;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int windowWidth = this.getWidth();
        int windowHeight = this.getHeight();

        int margin = 30;
        this.drawLabyrinth(g, margin, margin, windowWidth - 2*margin, windowHeight - 2 * margin);
        this.drawPlayer(g, margin, margin, windowWidth - 2*margin, windowHeight - 2* margin);
    }

    private void drawPlayer(Graphics g, int boardX, int boardY, int boardWidth, int boardHeight) {
        Location pLocation = this.model.getPlayerLocation();

        int rows = this.model.getBoard().numRows();
        int cols = this.model.getBoard().numColumns();

        int row = pLocation.row;
        int col = pLocation.col;

        int y = boardX + row * boardHeight / rows;
        int x = boardY + col * boardWidth / cols;
        int nexty = boardX + (row + 1) * boardHeight / rows;
        int nextx = boardY + (col + 1) * boardWidth / cols;
        int width = nextx - x;
        int height = nexty - y;

        paintMyCell(g, x, y, width, height, 8, Color.BLUE);
    }

    private void drawLabyrinth(Graphics g, int boardX, int boardY, int boardWidth, int boardHeight) {

        int rows = this.model.getBoard().numRows();
        int cols = this.model.getBoard().numColumns();


        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {

                Color color = getColor(row, col);

                int y = boardX + row * boardHeight / rows;
                int x = boardY + col * boardWidth / cols;
                int nexty = boardX + (row + 1) * boardHeight / rows;
                int nextx = boardY + (col + 1) * boardWidth / cols;
                int width = nextx - x;
                int height = nexty - y;

                this.paintMyCell(g, x, y, width, height, 8, color);
            }
        }
    }

    private Color getColor(int row, int col) {
        Character c = this.model.getBoard().get(new Location(row, col));

        if (c == 'w') {
            return Color.WHITE;
        }
        else if (c == '-') {
            return Color.BLACK;
        }
        else if (c == 'g') {
            return Color.GREEN;
        }

        return null;
    }

    private void paintMyCell(Graphics g, int x, int y, int width, int height, int margin, Color color) {
        g.setColor(color);
        g.fillRect(x, y, width-margin, height-margin);
    }
}
