package inf101v22.model;

import inf101v22.grid.Grid;
import inf101v22.grid.GridDirection;
import inf101v22.grid.IGrid;
import inf101v22.grid.Location;

public class Labyrinth {

    private IGrid<Character> board;
    private Location playerLocation;

    static IGrid<Character> getSampleBoard() {
        IGrid<Character> board = new Grid<>(6, 10, '-');
        board.set(new Location(0, 0), 'w');
        board.set(new Location(5, 9), 'g');

        return board;
    }

    public Labyrinth() {
        this.board = getSampleBoard();
        this.playerLocation = new Location(0, 0);
    }

    public IGrid<Character> getBoard() {
        return this.board;
    }

    public Location getPlayerLocation() {
        return this.playerLocation;
    }

    public void moveDown() {
        this.playerLocation = GridDirection.SOUTH.getNeighbor(this.playerLocation);
    }

}
